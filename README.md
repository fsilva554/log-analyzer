# Log analyzer

## Usage

Make sure to create a `.env` file, and set `LOG_FOLDER` to the location of the log files you want to
analyze

The only argument you need is the file's name you want to analyze.


### Optionals

There's a branch `optionals`, which include a Dockerfile, and the needed changes to run this tool as
a REST api.

First build the docker image:

`docker build -t log-analyzer .`

Then you must bind the log folder when running, like so:

`docker run -it -v LOG_FOLDER:/var/log/app -p 8080:8080 --env PORT=8080 --env LOG_FOLDER=/var/log/app
log-analyzer`

Notice how the `LOG_FOLDER` env var is set to the destination of the mount.

That's it! Enjoy!
