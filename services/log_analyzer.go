package services

import (
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"strings"
)

type LogStat struct {
	ServiceName            string
	InstanceWithMostErrors string
	AmountErrorsService    int
	AmountErrorsInstance   int
}

type LogAnalyzer struct {
	LogFolder string
}

func (l *LogAnalyzer) Run(filename string) ([]LogStat, error) {

	//Regexp for service and instance extraction from log line
	serviceInstanceRegex := regexp.MustCompile(`(\[[0-9A-Za-z_-]+\s[0-9A-Za-z_]+\])`)
	result := make([]LogStat, 0)
	aggr := make(map[string]map[string]int, 0)

	b, err := ioutil.ReadFile(l.LogFolder + "/" + filename)

	if err != nil {
		return result, errors.New(fmt.Sprint("Error reading file: ", err))
	}

	n := len(b)
	content := string(b[:n])

	for _, line := range strings.Split(content, "\n") {
		//Check if this line is an error
		match, _ := regexp.MatchString(`\[error\]`, line)
		if match {

			//Get service and instance from the line, removing brackets
			info := strings.Split(serviceInstanceRegex.FindStringSubmatch(line)[0], " ")
			service := strings.TrimPrefix(info[0], "[")
			instance := strings.TrimSuffix(info[1], "]")

			//Group error count by service and instance
			if _, ok := aggr[service]; !ok {
				aggr[service] = make(map[string]int, 0)
			}
			aggr[service][instance] += 1
		}
	}

	//Process aggregation from file
	for service, instances := range aggr {
		logStat := LogStat{ServiceName: service}
		for instance, errorCount := range instances {
			logStat.AmountErrorsService += errorCount
			if errorCount > logStat.AmountErrorsInstance {
				logStat.InstanceWithMostErrors = instance
				logStat.AmountErrorsInstance = errorCount
			}
		}
		result = append(result, logStat)
	}

	return result, nil

}
