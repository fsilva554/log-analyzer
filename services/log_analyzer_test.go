package services

import (
	"testing"
)

func TestLogFileNotFound(t *testing.T) {
	service := LogAnalyzer{"../"}
	_, err := service.Run("file")

	if err == nil {
		t.Fatal("Expected error, got nil")
	}
}

func TestLogFileContainsNoErrors(t *testing.T) {

	service := LogAnalyzer{"../"}
	result, err := service.Run("../test_log1.txt")

	if err != nil {
		t.Fatal("Expected clean execution, got error:", err)
	}

	amountResults := len(result)
	if amountResults > 0 {
		t.Fatal("Expected no results, got:", amountResults)
	}

}

func TestLogFileContainsErrorsForSingleServiceAndSingleInstance(t *testing.T) {

	expectedService := "api-gateway"
	expectedInstance := "ffd3082fe09d"
	expectedInstanceCount := 17
	expectedServiceCount := 17

	service := LogAnalyzer{"../"}
	result, err := service.Run("../test_log2.txt")

	if err != nil {
		t.Fatal("Expected clean execution, got error:", err)
	}

	amountResults := len(result)
	if amountResults != 1 {
		t.Fatal("Expected a single result, got:", amountResults)
	}

	if result[0].ServiceName != expectedService {
		t.Fatalf("Expected %s to be in the result list", expectedService)
	}

	if result[0].InstanceWithMostErrors != expectedInstance {
		t.Fatalf("Expected %s to be the instance with most errors", expectedInstance)
	}

	if result[0].AmountErrorsInstance != expectedInstanceCount {
		t.Fatalf("Expected %d errors for %s, got %d", expectedInstanceCount, expectedInstance, result[0].AmountErrorsInstance)
	}

	if result[0].AmountErrorsService != expectedServiceCount {
		t.Fatalf("Expected %d errors for %s, got %d", expectedServiceCount, expectedService, result[0].AmountErrorsService)
	}

}

func TestLogFileContainsErrorsForSingleServiceAndMultipleInstances(t *testing.T) {
	expectedService := "api-gateway"
	expectedInstance := "ffd3082fe09d"
	expectedInstanceCount := 14
	expectedServiceCount := 17

	service := LogAnalyzer{"../"}
	result, err := service.Run("../test_log3.txt")

	if err != nil {
		t.Fatal("Expected clean execution, got error:", err)
	}

	amountResults := len(result)
	if amountResults != 1 {
		t.Fatal("Expected a single result, got:", amountResults)
	}

	if result[0].ServiceName != expectedService {
		t.Fatalf("Expected %s to be in the result list", expectedService)
	}

	if result[0].InstanceWithMostErrors != expectedInstance {
		t.Fatalf("Expected %s to be the instance with most errors", expectedInstance)
	}

	if result[0].AmountErrorsInstance != expectedInstanceCount {
		t.Fatalf("Expected %d errors for %s, got %d", expectedInstanceCount, expectedInstance, result[0].AmountErrorsInstance)
	}

	if result[0].AmountErrorsService != expectedServiceCount {
		t.Fatalf("Expected %d errors for %s, got %d", expectedServiceCount, expectedService, result[0].AmountErrorsService)
	}

}

func TestLogFileContainsErrorsForMultipleServicesAndMultipleInstances(t *testing.T) {
	expectedService1 := "api-gateway"
	expectedService2 := "payment-processor"
	expectedInstance1 := "ffd3082fe09d"
	expectedInstance2 := "15c3232fe1ac"
	expectedInstance1Count := 9
	expectedInstance2Count := 4
	expectedService1Count := 12
	expectedService2Count := 5

	expectedResults := make(map[string]map[string]int, 0)

	expectedResults[expectedService1] = make(map[string]int, 0)
	expectedResults[expectedService1][expectedInstance1] = expectedInstance1Count
	expectedResults[expectedService1]["count"] = expectedService1Count

	expectedResults[expectedService2] = make(map[string]int, 0)
	expectedResults[expectedService2][expectedInstance2] = expectedInstance2Count
	expectedResults[expectedService2]["count"] = expectedService2Count

	service := LogAnalyzer{"../"}
	result, err := service.Run("../test_log4.txt")

	if err != nil {
		t.Fatal("Expected clean execution, got error:", err)
	}

	amountResults := len(result)
	expectedAmountResults := len(expectedResults)
	if amountResults != expectedAmountResults {
		t.Fatalf("Expected %d results, got: %d", expectedAmountResults, amountResults)
	}

	for _, logStat := range result {
		if _, ok := expectedResults[logStat.ServiceName]; !ok {
			t.Fatalf("Didn't expect %s to be in the result list", logStat.ServiceName)
		}
		if _, ok := expectedResults[logStat.ServiceName][logStat.InstanceWithMostErrors]; !ok {
			t.Fatalf("Didn't expect %s to be the instance with most errors for %s list", logStat.InstanceWithMostErrors, logStat.ServiceName)
		}
		expectedInstanceErrors := expectedResults[logStat.ServiceName][logStat.InstanceWithMostErrors]
		if expectedInstanceErrors != logStat.AmountErrorsInstance {
			t.Fatalf("Expected %d errors for %s, got %d", expectedInstanceErrors, logStat.InstanceWithMostErrors, logStat.AmountErrorsInstance)
		}

		expectedServiceErrors := expectedResults[logStat.ServiceName]["count"]
		if expectedServiceErrors != logStat.AmountErrorsService {
			t.Fatalf("Expected %d errors for %s, got %d", expectedServiceErrors, logStat.ServiceName, logStat.AmountErrorsService)
		}
	}

}
