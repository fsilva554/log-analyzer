package main

import (
	"fmt"
	"log"
	"os"
	"pager-log-analytics/services"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	logFolder := os.Getenv("LOG_FOLDER")

	s := services.LogAnalyzer{
		LogFolder: logFolder,
	}

	result, err := s.Run(os.Args[1])

	if err != nil {
		log.Fatal(err)
		return
	}

	for _, logStat := range result {
		s := fmt.Sprintf("Service %s, total errors: %d, instance with most errors: %s (%d errors)", logStat.ServiceName, logStat.AmountErrorsService, logStat.InstanceWithMostErrors, logStat.AmountErrorsInstance)
		fmt.Println(s)
	}

}
